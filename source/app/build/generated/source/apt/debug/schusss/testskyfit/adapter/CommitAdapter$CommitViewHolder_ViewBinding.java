// Generated code from Butter Knife. Do not modify!
package schusss.testskyfit.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;
import schusss.testskyfit.R;

public class CommitAdapter$CommitViewHolder_ViewBinding<T extends CommitAdapter.CommitViewHolder> implements Unbinder {
  protected T target;

  @UiThread
  public CommitAdapter$CommitViewHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.commitAuthorThumbnail = Utils.findRequiredViewAsType(source, R.id.commit_author_thumb, "field 'commitAuthorThumbnail'", ImageView.class);
    target.commitAuthor = Utils.findRequiredViewAsType(source, R.id.commit_author, "field 'commitAuthor'", TextView.class);
    target.commitSha = Utils.findRequiredViewAsType(source, R.id.commit_sha, "field 'commitSha'", TextView.class);
    target.commitMessage = Utils.findRequiredViewAsType(source, R.id.commit_message, "field 'commitMessage'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.commitAuthorThumbnail = null;
    target.commitAuthor = null;
    target.commitSha = null;
    target.commitMessage = null;

    this.target = null;
  }
}
