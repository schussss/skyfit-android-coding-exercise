// Generated code from Butter Knife. Do not modify!
package schusss.testskyfit.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;
import schusss.testskyfit.R;

public class MainActivity_ViewBinding<T extends MainActivity> implements Unbinder {
  protected T target;

  private View view2131492973;

  @UiThread
  public MainActivity_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.mRecylerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'mRecylerView'", RecyclerView.class);
    target.mRefresh = Utils.findRequiredViewAsType(source, R.id.refresh, "field 'mRefresh'", SwipeRefreshLayout.class);
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progress, "field 'progressBar'", ProgressBar.class);
    view = Utils.findRequiredView(source, R.id.retry, "field 'retry' and method 'retry'");
    target.retry = Utils.castView(view, R.id.retry, "field 'retry'", Button.class);
    view2131492973 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.retry();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mRecylerView = null;
    target.mRefresh = null;
    target.progressBar = null;
    target.retry = null;

    view2131492973.setOnClickListener(null);
    view2131492973 = null;

    this.target = null;
  }
}
