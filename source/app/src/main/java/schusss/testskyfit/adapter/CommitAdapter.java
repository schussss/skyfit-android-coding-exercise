package schusss.testskyfit.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import schusss.testskyfit.R;
import schusss.testskyfit.data.Commit;
import schusss.testskyfit.helper.CircleTransform;

/**
 * Created by max on 04/10/2016.
 */

public class CommitAdapter extends RecyclerView.Adapter<CommitAdapter.CommitViewHolder> {
    private final CircleTransform circleTransformation;
    private List<Commit> mCommits;
    private OnItemClickListener mListener;

    public CommitAdapter(List<Commit> commits, OnItemClickListener listener) {
        mCommits = commits;
        mListener = listener;
        circleTransformation = new CircleTransform();
    }

    @Override
    public CommitViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CommitViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_commit, parent, false));
    }

    @Override
    public void onBindViewHolder(CommitViewHolder holder, int position) {
        final Commit content = mCommits.get(position);
        holder.commitSha.setText(content.getSha());
        holder.commitAuthor.setText(content.getCommit().getAuthor().getName());
        holder.commitMessage.setText(content.getCommit().getMessage());
        Picasso.with(holder.itemView.getContext()).load(content.getAuthor().getAvatarUrl()).transform(circleTransformation).into(holder.commitAuthorThumbnail);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onItemClicked(content);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCommits.size();
    }

    public void add(List<Commit> newContent) {
        for (Commit v : newContent) {
            mCommits.add(v);
            notifyItemInserted(mCommits.size());
        }
    }

    public static class CommitViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.commit_author_thumb)
        ImageView commitAuthorThumbnail;

        @BindView(R.id.commit_author)
        TextView commitAuthor;

        @BindView(R.id.commit_sha)
        TextView commitSha;

        @BindView(R.id.commit_message)
        TextView commitMessage;

        public CommitViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}