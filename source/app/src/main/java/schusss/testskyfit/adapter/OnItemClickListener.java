package schusss.testskyfit.adapter;

/**
 * Created by max on 04/10/2016.
 */

public interface OnItemClickListener<T> {
    void onItemClicked(T item);
}