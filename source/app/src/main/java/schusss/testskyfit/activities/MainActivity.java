package schusss.testskyfit.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import schusss.testskyfit.R;
import schusss.testskyfit.adapter.CommitAdapter;
import schusss.testskyfit.adapter.EndlessRecyclerOnScrollListener;
import schusss.testskyfit.adapter.OnItemClickListener;
import schusss.testskyfit.api.ApiUtil;
import schusss.testskyfit.api.NetClient;
import schusss.testskyfit.data.Commit;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, OnItemClickListener<Commit> {


    @BindView(R.id.recyclerView)
    RecyclerView mRecylerView;

    @BindView(R.id.refresh)
    SwipeRefreshLayout mRefresh;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @BindView(R.id.retry)
    Button retry;

    @OnClick(R.id.retry)
    void retry() {
        progressBar.setVisibility(View.VISIBLE);
        fetchCommits();
    }

    int page = 1;
    ApiUtil api = NetClient.apiUtil();
    private CommitAdapter mAdapter;
    private EndlessRecyclerOnScrollListener loadMoreListener;
    private boolean mHasMoreData = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
    }

    private void initData() {
        ButterKnife.bind(this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecylerView.setLayoutManager(mLayoutManager);
        loadMoreListener = new EndlessRecyclerOnScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int currentPage) {
                if (!mHasMoreData) return;
                page++;
                fetchCommits();
            }
        };
        mRecylerView.addOnScrollListener(loadMoreListener);
        mRefresh.setOnRefreshListener(this);
        progressBar.setVisibility(View.VISIBLE);
        fetchCommits();
    }


    @Override
    public void onRefresh() {
        loadMoreListener.reset();
        page = 1;
        fetchCommits();
    }


    private void fetchCommits() {
        retry.setVisibility(View.GONE);
        if (NetClient.isOnline(getApplicationContext())) {
            //device is online
            api.fetchCommits(page)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .doOnError(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            mRefresh.setRefreshing(false);
                            Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            mHasMoreData = false;
                        }
                    })
                    .subscribe(new Action1<List<Commit>>() {
                        @Override
                        public void call(List<Commit> commits) {
                            progressBar.setVisibility(View.GONE);
                            mRefresh.setRefreshing(false);
                            mHasMoreData = commits != null && commits.size() > 0;
                            if (mAdapter != null && page > 1)
                                mAdapter.add(commits);
                            else
                                displayCommitRequest(commits);
                        }
                    });
        } else {
            //device is offline and there it's the first use of the app
            Toast.makeText(getApplicationContext(), R.string.no_internet, Toast.LENGTH_SHORT).show();
            displayRetry(R.string.retry);
        }

    }


    public void displayCommitRequest(List<Commit> commits) {
        if (commits == null) {
            //error with process
            displayRetry(R.string.error_parse);
            return;
        } else if (commits.size() == 0) {
            //empty list of user
            displayRetry(R.string.no_commits);
            return;
        }
        mAdapter = new CommitAdapter(commits, this);
        mRecylerView.setAdapter(mAdapter);
    }

    private void displayRetry(int message) {
        retry.setText(message);
        retry.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }


    @Override
    public void onItemClicked(Commit item) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getHtmlUrl()));
        startActivity(browserIntent);
    }


}
