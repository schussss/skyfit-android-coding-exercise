package schusss.testskyfit.api;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;
import schusss.testskyfit.data.Commit;

/**
 * Created by max on 04/10/2016.
 */

public interface ApiUtil {

    @GET("/repos/rails/rails/commits")
    Observable<List<Commit>> fetchCommits(@Query("page") int page);
}
