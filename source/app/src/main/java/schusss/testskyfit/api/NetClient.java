package schusss.testskyfit.api;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by max on 04/10/2016.
 */

public class NetClient {
    public NetClient() {

    }

    static String baseUrl() {
        return "http://api.github.com";
    }

    static Retrofit retrofit() {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(baseUrl())
                .build();
    }

    public static ApiUtil apiUtil() {
        return retrofit().create(ApiUtil.class);
    }


    public static boolean isOnline(Context ctx) {
        if (ctx != null) {
            try {
                ConnectivityManager cm =
                        (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                return netInfo != null && netInfo.isConnectedOrConnecting();
            } catch (Exception e) {
                Log.e("TESTSKYFIT","error while checking connection status " + e.getMessage());
                return false;
            }
        }
        return false;
    }
}
